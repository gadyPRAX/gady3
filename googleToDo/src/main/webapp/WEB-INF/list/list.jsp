<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<h2>ToDo</h2>


	<table>
		<caption>ToDos</caption>
		<tr>
			<th>Name</th>
		</tr>

		<c:forEach var="list" items="${lists}">
			<tr>
				<td>${list.title}</td>
				
				<c:forEach var="operation" items="${operations.entrySet()}">
				<td><a href="${pageContext.request.contextPath}/list/${operation.key}?id=${list.id}">${operation.value}</a></td>
				</c:forEach>
			</tr>

		</c:forEach>
	</table>


	<form action="${pageContext.request.contextPath}/list/create" method="post">
		<input name="title" type="text"> <input type="submit"
			name="Crear">
	</form>


</body>
</html>