<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>UpdateList: ${toUpdate.title}	</title>
</head>
<body>

<form action="${pageContext.request.contextPath}/list/update" method="post">
<fieldset>
<legend>UpdateList: ${toUpdate.title}</legend>
New title: <input type="text" name="title" value="${toUpdate.title}" placeholder="some title">
<input type="hidden" value="${toUpdate.id}" name="id" />
<input type="submit" value="Update">
</fieldset>
</form>

</body>
</html>