<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Detail: ${listDetail.title}</title>
</head>
<body>
<h3>Id: ${listDetail.id}</h3>
<table>
<caption>Tasks</caption>
<tr>
<th>Name</th>
</tr>

<c:forEach var="task" items="${listDetail.tasks}">
<tr>
<td>${task.title}</td>
<c:forEach var="operation" items="${operation.entrySet()}">
<td><a href="${pageContext.request.contextPath}/task/${operation.key}?id=${task.id}">${operation.value}</a></td>
</c:forEach>
</tr>
</c:forEach>

</table>

<a href="${pageContext.request.contextPath}/">Return to all list</a>
</body>
</html>