package mx.com.praxis.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import mx.com.praxis.daos.ListDao;
import mx.com.praxis.daos.NoteDao;
import mx.com.praxis.daos.TaskDao;
import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.services.ListService;
import mx.com.praxis.utils.Converter;

@Stateless
public class ListServiceImpl implements ListService{
	
	@Inject
	private ListDao listDao;
	
	@Inject
	private TaskDao taskDao;
	
	@Inject
	private NoteDao noteDao;

	@Override
	public List<ListDto> getAllListDto() {
		return Converter.convertListsToListDtos(listDao.findAll());
	}

	@Override
	public void create(ListDto listDto) {
		listDao.create(Converter.convertListDtoToList(listDto));		
	}

	@Override
	public ListDto getListDtoById(Long id) {
		return Converter.convertListToListDto(listDao.findByID(id));
	}

	@Override
	public void update(ListDto listDto) {
		listDao.update(Converter.convertListDtoToListUpdate(listDto));
		
	}

	@Override
	public void delete(Long id) {
		taskDao.findTaskByIdList(id)
		.forEach(t -> {
			noteDao.findNoteByIdTask(t.getId())
			.forEach(noteDao::delete);
			taskDao.delete(t);
		});
		
		listDao.delete(listDao.findByID(id));
		
	}

}
