package mx.com.praxis.controlles;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.services.ListService;
import mx.com.praxis.utils.Converter;

@WebServlet(urlPatterns = {
	"/list/create",
	"/list/read",
	"/list/update",
	"/list/delete"
})
public class ListController extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 234211902562245783L;
	@Inject
	private ListService listService;
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String operation = request.getServletPath();
				
		if("/list/update".equals(operation)) {
			updateListGet(request, response);
		}

		if("/list/delete".equals(operation)) {
			deleteList(request, response);
		}
		
	}
	
	private void updateListGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		request.setAttribute("toUpdate", listService.getListDtoById(id));
		request.getRequestDispatcher("/WEB-INF/list/listUpdate.jsp").forward(request, response);
	}
	
	private void createList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ListDto toCreate = Converter.convertRequestToListDto(request);
		listService.create(toCreate);
		
		String url = request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void updateList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ListDto toUpdate = Converter.convertRequestToListDtoUpdate(request);
		listService.update(toUpdate);
		
		String url = request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void deleteList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		listService.delete(Long.parseLong(request.getParameter("id")));
		
		String url = request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void readList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		listService.delete(Long.parseLong(request.getParameter("id")));
		
		String url = request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String operation = request.getServletPath();
		
		if("/list/create".equals(operation)) {
			createList(request, response);
		}
		
		if("/list/update".equals(operation)) {
			updateList(request, response);
		}
		
	}
}
