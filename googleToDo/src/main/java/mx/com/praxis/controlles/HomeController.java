package mx.com.praxis.controlles;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.services.ListService;

@WebServlet(urlPatterns = {
		"/"
})
public class HomeController extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3111106719677824854L;

	@Inject
	private ListService listService;
	
	private static final Map<String, String> OP = new HashMap<>(3);
	
	static {
		OP.put("read", "Read");
		OP.put("update", "Update");
		OP.put("delete", "Delete");
	}
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("operations", OP);
		request.setAttribute("lists", listService.getAllListDto());
		request.getRequestDispatcher("/WEB-INF/list/list.jsp").forward(request, response);
	}

}

