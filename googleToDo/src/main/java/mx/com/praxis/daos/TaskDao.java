package mx.com.praxis.daos;

import java.util.List;

import javax.ejb.Local;

import mx.com.praxis.entities.Task;

@Local
public interface TaskDao extends GenericDao<Task, Long>{
	public List<Task> findTaskByIdList(Long id);
	

}
