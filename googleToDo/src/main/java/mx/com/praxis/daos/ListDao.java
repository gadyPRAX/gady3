package mx.com.praxis.daos;

import javax.ejb.Local;

import mx.com.praxis.entities.List;

@Local
public interface ListDao extends GenericDao<List, Long>{

}
