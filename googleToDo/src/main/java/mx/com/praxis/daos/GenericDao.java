package mx.com.praxis.daos;

import java.util.List;

public interface GenericDao<T, K> {
	
	public List<T> findAll();
	
	public T findByID(K id);
	
	public void update(T entity);
	
	public void create(T entity);
	
	public void delete(T entity);
}
