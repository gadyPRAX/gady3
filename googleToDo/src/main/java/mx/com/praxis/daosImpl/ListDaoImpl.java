package mx.com.praxis.daosImpl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.praxis.daos.ListDao;

@Stateless
public class ListDaoImpl implements ListDao{
	
	@PersistenceContext(name = "AccessDataBase")
	private EntityManager entityManager;

	@Override
	public List<mx.com.praxis.entities.List> findAll() {
		TypedQuery<mx.com.praxis.entities.List> queryToFindAllList = entityManager.createNamedQuery("List.findAll", mx.com.praxis.entities.List.class);
		return queryToFindAllList.getResultList();
	}

	@Override
	public mx.com.praxis.entities.List findByID(Long id) {
		TypedQuery<mx.com.praxis.entities.List> queryToFindAllList = entityManager.createNamedQuery("List.findById", mx.com.praxis.entities.List.class);
		queryToFindAllList.setParameter("id", id);
		return queryToFindAllList.getSingleResult();
	}

	@Override
	public void update(mx.com.praxis.entities.List entity) {
		entityManager.merge(entity);
		
	}

	@Override
	public void create(mx.com.praxis.entities.List entity) {
		entityManager.persist(entity);
		
	}

	@Override
	public void delete(mx.com.praxis.entities.List entity) {
		entityManager.remove(entity);
	}

}
