package mx.com.praxis.daosImpl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.praxis.daos.NoteDao;
import mx.com.praxis.entities.Note;

@Stateless
public class NoteDaoImpl implements NoteDao{

	@PersistenceContext(name = "AccessDataBase")
	private EntityManager entityManager;
	
	@Override
	public List<Note> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Note findByID(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Note entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void create(Note entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Note entity) {
		entityManager.remove(entity);
		
	}

	@Override
	public List<Note> findNoteByIdTask(Long id) {
		TypedQuery<Note> queryToFindAllNoteByIdTask = entityManager.createNamedQuery("Note.findAllNoteByIdTask", Note.class);
		queryToFindAllNoteByIdTask.setParameter("idTask", id);
		return queryToFindAllNoteByIdTask.getResultList();
	}
	
}
