package mx.com.praxis.daosImpl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.praxis.daos.TaskDao;
import mx.com.praxis.entities.Task;

@Stateless
public class TaskDaoImpl implements TaskDao{

	@PersistenceContext(name = "AccessDataBase")
	private EntityManager entityManager;
	
	@Override
	public List<Task> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Task findByID(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update(Task entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void create(Task entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Task entity) {
		entityManager.remove(entity);		
	}

	@Override
	public List<Task> findTaskByIdList(Long id) {
		TypedQuery<Task> queryToFindAllTaskByIdList = entityManager.createNamedQuery("Task.findAllTaskByIdList", Task.class);
		queryToFindAllTaskByIdList.setParameter("idList", id);
		return queryToFindAllTaskByIdList.getResultList();
	}

}
