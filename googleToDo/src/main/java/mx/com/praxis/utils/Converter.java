package mx.com.praxis.utils;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import mx.com.praxis.dtos.ListDto;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Converter {
	public static List<ListDto> convertListsToListDtos(List<mx.com.praxis.entities.List> source){
		return source
				.stream()
				.map(Converter::convertListToListDto)
				.collect(Collectors.toList());
	}
	
	public static ListDto convertListToListDto(mx.com.praxis.entities.List source){
		return ListDto.builder()
				.id(source.getId())
				.title(source.getTitle())
				.build();
	}
	
	public static mx.com.praxis.entities.List convertListDtoToList(ListDto source){
		return mx.com.praxis.entities.List.builder()
				.title(source.getTitle())
				.build();
	}
	
	public static ListDto convertRequestToListDto(HttpServletRequest request){
		return ListDto.builder()
				.title(request.getParameter("title"))
				.build();
	}
	
	public static ListDto convertRequestToListDtoUpdate(HttpServletRequest request){
		return ListDto.builder()
				.id(Long.parseLong(request.getParameter("id")))
				.title(request.getParameter("title"))
				.build();
	}
	
	public static mx.com.praxis.entities.List convertListDtoToListUpdate(ListDto source){
		return mx.com.praxis.entities.List.builder()
				.id(source.getId())
				.title(source.getTitle())
				.build();
	}
}
